% !TeX spellcheck = <none>
\chapter{An Introduction to the Emulator Modelling Step Discontinuous Functions} \label{Chapter:IntroductionToTheEmulator}
%\todo[inline]{think of a better title + motivation! (and move this section to the beginning)}

This thesis investigates how the emulator models step discontinuous functions. The Heaviside function will be used to start with, as it is a simple function with its main property being a step discontinuity. Equation \ref{eq:HS_function} is the Heaviside function for this thesis.

\begin{equation} \label{eq:HS_function}
	\eta\left(x\right)=\left\{ \begin{array}{ccc}
		0 &  & x<0\\
		1 &  & x\geq0
	\end{array}\right.
\end{equation}

The correlation function used is from equation \ref{eq:correlationOakley}: $  c\left(\mathbf{x},\mathbf{x}'\right)=e^{-\left(\mathbf{x}\mathbf{-x'}\right)^{T}B\left(\mathbf{x}-\mathbf{x'}\right)}$, which is commonly used from other research, $B$ will be equal to $1$. Because the Heaviside function has no slope, the regression function will be a constant number $\textbf{h}\left(x\right)=1$. Both the correlation and regression function will been discussed in more detail in Section \ref{Experiment_Setup_Assumptions} (page \pageref{Experiment_Setup_Assumptions}).

This chapter will provide some visual analysis of the emulator modelling step discontinuous functions using simple approaches. Data points will be chosen with at least one near the discontinuity and "stitched"\footnote{The word stitched in this context means to have two points evaluated close to and either side of the discontinuity} at the discontinuity. Some of the goodness-of-fit measures will be applied to visually measure the accuracy of the emulator.

\newpage
The below table is a summary of examples of the figures that will be discussed in this chapter. The purpose of these figures and their discussion is to give an insight on some techniques and its limitations on how the emulator models functions like the Heaviside.


% ....
\begin{table} [H]
	
	\centering
	\begin{tabularx}{\linewidth}{|X|l|}
		\hline
		\textbf{Figure} & \textbf{description} \\
		\hline
		Figure \ref{fig:emulatorexampleonhs9} & Using a continuous constant regressor to model the Heaviside \\&function with no stitching\\
		\hline
		Figure \ref{fig:emulatorexampleonhs} & The discontinuity's position is not known,\\&
		however one data point is close to the discontinuity (left side)\\
		\hline
		Figure \ref{fig:emulatorexampleonhs2} & The discontinuity's position is not known,\\&
		however one data point is close to the discontinuity (right side)\\
		\hline
		Figure \ref{fig:emulatorexampleonhs3} & The discontinuity's position is effectively known,\\&
		a continuous regressor is still used and is now "stitched"; two data\\&
		points are evaluated close to and either side of the discontinuity\\
		\hline
		Figure \ref{fig:emulatorexampleonhs5} & Using a discontinuous regressor, the discontinuity is partly known, \\&
		with one point close to the  discontinuity (similar to Figure \ref{fig:emulatorexampleonhs}) \\
		\hline
		Figure \ref{fig:emulatorexampleonhs4} & Using the same discontinuous regressor as in Figure \ref{fig:emulatorexampleonhs5},\\&
											    with two data points "stitched" at the discontinuity\\
		\hline
		Figure \ref{fig:emulatorexampleonhs7} & Using a discontinuous regressor to model the Heaviside function \\&
		with no stitching, however the location of the discontinuity\\& is provided from the regressor\\
		\hline
		
		Figure \ref{fig:emulatorexampleonhs8}  & Using a discontinuous regressor to model the Heaviside function \\(a \& b)&with no stitching, and the location of the discontinuity\\& is inferred from the data points\\
		\hline
		
		Figure \ref{fig:emulatorexampleonhs10} & Using a discontinuous regressor to model the Heaviside function \\&with no stitching, and the location of the discontinuity\\& is incorrect\\
		\hline
		
		
	\end{tabularx}
\caption{Summary of the Figures}
\end{table}

\newpage

%\begin{tabular}{|c|c|}
%	\hline 
%	& description\tabularnewline
%	\hline 
%	\hline 
%	Figure \ref{fig:emulatorexampleonhs} & The discontinuity's position is not known, however one data point is close to the discontinuity (left side)
%	\tabularnewline\hline 
%	Figure \ref{fig:emulatorexampleonhs2} & The discontinuity's position is not known, however one data point is close to the discontinuity (right side)
%	\tabularnewline\hline 
%	& \tabularnewline
%	\hline 
%	& \tabularnewline
%	\hline 
%	& \tabularnewline
%	\hline 
%	& \tabularnewline
%	\hline 
%	& \tabularnewline
%	\hline 
%	& \tabularnewline
%	\hline 
%	& \tabularnewline
%	\hline 
%\end{tabular}



\section{Example 1: One data point at the \\discontinuity} 

Starting with Figure \ref{fig:emulatorexampleonhs} (see page \pageref{fig:emulatorexampleonhs}), where; $-3 \leq x \leq -0.05$ and $1 \leq x \leq 3$ the emulator is a reasonable representation of the Heaviside function as if the true function is smooth. For this Heaviside function the slope is always positive between the two data points on either side of the discontinuity (even though we don't know where it is) and there is no over/under shooting of the function between these points. Estimating the location of the discontinuity by $\hat{\eta}=0.5$ gives a location of 0.5.


Figure \ref{fig:emulatorexampleonhs2} (see page \pageref{fig:emulatorexampleonhs2}) is obtained with the only change from Figure \ref{fig:emulatorexampleonhs} (page \pageref{fig:emulatorexampleonhs}) of the one data point at the discontinuity. Estimating the location of the discontinuity by $\hat{\eta}=0.5$ gives a location of -0.5. Because the data points are symmetric, the approximated location of the discontinuity is also symmetric to Figure \ref{fig:emulatorexampleonhs}. Between the domain of 0 to 3 it can be seen that the emulator is a reasonable representation of the Heaviside function. As expected, the emulator will always converge to the prior outside of the data points due to the lack of information (no data points).


\section{Example 2: Two data point stitched at the discontinuity}

For Figure \ref{fig:emulatorexampleonhs3} (see page \pageref{fig:emulatorexampleonhs3}), there are two data points very close on either side on the discontinuity, giving a simple strategy of using the data points to manipulate the emulator, as if the location of the discontinuity is almost certain. This has made the emulator worse compared to Figure \ref{fig:emulatorexampleonhs} (page \pageref{fig:emulatorexampleonhs}) and \ref{fig:emulatorexampleonhs2} (page \pageref{fig:emulatorexampleonhs2}), causing the emulator to be very inaccurate in predicting the function with a lot of over/under shooting The slope of the emulator is high at the point of the discontinuity which has caused the extreme over and under shooting and the closer the two points are the larger the slope would be.
		
Note that the extreme over/under shooting could be reduced by changing $B$ in the correlation function, however because the Heaviside function has no length scale $B$ is kept at $1$


\section{Example 3: Using a discontinuous function as the regressor, with one data point at the discontinuity}


If we were to know the location of the discontinuity, then everything changes; our current setup with the regressor and correlation function might have to change (particularly the regressor function as seen in Section \ref{sssec:WhyNoDisRegessionFunction}). To demonstrate this, let $\eta\left(x\right)=\left\{ \begin{array}{ccc}
2x+5 &  & x\leq0\\
\frac{x}{2}-3 &  & x>0
\end{array}\right.$, and the regressor function to be $h\left(x\right)=\left\{ \begin{array}{ccc}
0 &  & x<0\\
1 &  & x\geq0
\end{array}\right.$, and using the same data points as Figure \ref{fig:emulatorexampleonhs}.

From Figure \ref{fig:emulatorexampleonhs5}, the region outside of the data points of the discontinuity ($-3\leq x \leq 0$ and $1 \leq x \leq 3$) the emulator is a reasonable representation of $\eta$, however the emulator overshoots between $0$ and $1$.

The variance is also discontinuous due to equation \ref{eq:posteriorvariance} (page \pageref{eq:posteriorvariance}), where $\mbox{c}^{**}\left(\mathbf{x},\mathbf{x}'\right)$ contains the discontinuous regression function.



\section{Example 4: Using a discontinuous function as the regressor, with two data point stitched at the discontinuity}

Knowing that the discontinuity is at $0$ we obtain Figure \ref{fig:emulatorexampleonhs4}. Because we the data points either side of the discontinuity are close together there is very little over/under shoot. This shows that, if the data points are "stitched" at the discontinuity; it could be suitable to use a regressor that contains the discontinuity at the stitched location. (as compared to Figure \ref{fig:emulatorexampleonhs3}). As a side note of the emulator; because it is "stitched", the prior is restricted to be the same distance above each of the two stitched data points (in this case $3$).

\section{Example 4: Using a discontinuous function as the regressor to model the Heaviside Function} \label{sssec:WhyNoDisRegessionFunction}

Returning back to the Heaviside function, let the regressor now be: \\
$h\left(x\right)=\left\{ \begin{array}{ccc}
\frac{x}{2}-3 &  & x\leq0\\
2x+5 &  & x>0
\end{array}\right.$.
From this, Figure \ref{fig:emulatorexampleonhs7} is obtained. It is observed that with this prior, the emulator has shown less of an over/under shooting at the discontinuity, compared to when the constant regressor was used (Figure \ref{fig:emulatorexampleonhs9}). This is because information about the discontinuity has been provided to the emulator.

However, if the location of the discontinuity is unknown, then the only information available is the data points. Figure \ref{fig:emulatorexampleonhs8} demonstrates two extreme ends, assuming the discontinuity would be between the two data points in which "jumps" from $0$ to $1$. Comparing Figure \ref{fig:emulatorexampleonhs9} to Figure \ref{fig:emulatorexampleonhs8} it could be suggested that using a discontinuous regressor when the location of the discontinuity is unknown, does not significantly improve the emulator in modelling the Heaviside function.

When the data points are ignored to estimate where the discontinuity might be the emulator could result in something similar to Figure \ref{fig:emulatorexampleonhs10} that shows that, any information conflicting with the prior results in a high level of errors for the emulator causing over/under shooting.



 




\section{Conclusion}
%\todo{fix this}
The software of the emulator works as expected; however, Bayesian emulation and modelling of discontinuous functions can be a complex process alone and more so together. The emulator is able to provide a reasonable representation of the Heaviside function when using a constant regression function but does show a decrease in accuracy near the discontinuity. This inaccuracy at the discontinuity is due to multiple factors, particularly the regression function and correlation, but more so on the assumption that the location of the discontinuity is unknown.

From the literature, it has been suggested to run separate emulators for each region, removing the problem of having discontinuities \cite{Caiado2015123}. This chapter has demonstrated that it is possible to use one emulator by using a discontinuous regressor. However in both cases the discontinuity's location is known.

The emulator can be improved at modelling a discontinuous function by specifying the regressor function to include the discontinuity when the location of the discontinuity is known; in which Figure \ref{fig:emulatorexampleonhs4} has demonstrated. When the location of the discontinuity is unknown, the emulator only maintains acceptable accuracy outside the domain between the data points either side of the discontinuity.

This has shown how strongly dependent the emulator is on the regressor function, the correlation function, and the data points. It has been demonstrated that when using a discontinuous regressor, the emulator relies heavily on the assumption that the location of the discontinuity is known; however, there are times when it may be impractical (or effectivity impossible) to run the function/simulator multiple times to narrow down where the discontinuity is. 

For this reason; this thesis will be focusing on the emulator with unknown position of the discontinuity, by not providing the location of the discontinuity or that the function even has a discontinuity to the emulator. Until now, this thesis has looked at informal measures of the emulator performance. However, this thesis will discuss formal goodness-of-fit measures (as discuss in Chapter \ref{chap3});  goodness-of-fit techniques will be designed to measure the emulator's accuracy measuring upon the Heaviside function and other similar functions conducted in this research. These goodness-of-fit techniques are difficult to quantify objectively on what makes a "good" emulation in terms of being able to fit to a discontinuous function like the Heaviside. The goodness-of-fit techniques should measure certain properties from the discontinuous function such as the gradient at the point of the discontinuity.  


\newpage

\begin{landscape}
	\begin{figure}[ht]
		\centering
		\includegraphics[width=0.7\linewidth]{images/Emulator_example_on_HS_9}
		\caption[]
		{
			A simple example of the emulator modelling the Heaviside function with the data points symmetric.
			
			
		}
		\label{fig:emulatorexampleonhs9}
	\end{figure}
\end{landscape}

\begin{landscape}
	\begin{figure}[ht]
		\centering
		\includegraphics[width=0.7\linewidth]{images/Emulator_example_on_HS_new}
		\caption[Example of the emulator modelling the Heaviside function with one data point close to the discontinuity]
		{
		An example of the emulator modelling the Heaviside function with one data point close to the discontinuity:
		
		Where; $-3 \leq x \leq -0^{-}$ and $1 \leq x \leq 3$ the emulator is a reasonable representation of the Heaviside function as if the true function is smooth. For this Heaviside function the slope is always positive between the two data points on either side of the discontinuity (even though we don't know where it is) and there is no over/under shooting of the function between these points. 
%		Estimating the location of the discontinuity by $\hat{\eta}=0.5$ gives a location 0.5.
		
%		If an estimate of the discontinuity's was to be when  $\hat{\eta}=0.5$, then the location of the discontinuity would be about  0.5.
		}
		\label{fig:emulatorexampleonhs}
	\end{figure}
\end{landscape}



\newpage
\begin{landscape}
	\begin{figure}[ht]
	\centering
	\includegraphics[width=0.7\linewidth]{images/Emulator_example_on_HS_2}
	\caption[Example of the emulator modelling the Heaviside function with one data point close but on the other side to the discontinuity]
	{
		An example of the emulator modelling the Heaviside function with one data point close to but on the other side of the discontinuity:
		
		The only change from Figure \ref{fig:emulatorexampleonhs} is the one data point at the discontinuity. Between the domain of 0 to 3 it can be seen that the emulator is a reasonable representation of the Heaviside function. As expected, the emulator will always converge to the prior outside of the data points due to the lack of information (no data points).
	}
	\label{fig:emulatorexampleonhs2}
	\end{figure}
\end{landscape}

\newpage


\begin{landscape}
	\begin{figure}[ht]
	\centering
	\includegraphics[width=0.7\linewidth]{images/Emulator_example_on_HS_3_new}
	\caption[Example when using two data points on either side the discontinuity]
	{
		An Example when using two data points close on either side the discontinuity (note the y-axis and the extreme over/under shooting, legend is same as Figure \ref{fig:emulatorexampleonhs9}):
		
		The two data points are very close on either side to the discontinuity, giving a simple strategy of using the data points to manipulate the emulator,as if the location of the discontinuity is almost certain. This has made the emulator worse compared to Figure \ref{fig:emulatorexampleonhs} and \ref{fig:emulatorexampleonhs2}, causing the emulator to be very inaccurate in predicting the function with a lot of over/under shooting The slope of the emulator is high at the point of the discontinuity which has caused the extreme over and under shooting and the closer the two points are the larger the slope would be.
		
%		Note that the extreme over/under shooting could be reduced by changing $B$ in the correlation function, however because the Heaviside function has no length scale $B$ is kept at $1$
	}
	\label{fig:emulatorexampleonhs3}
	\end{figure}
\end{landscape}

\newpage

\begin{landscape}
	\begin{figure}[ht]
	\centering
	\includegraphics[width=0.65\linewidth]{images/Emulator_example_on_HS_5}
	\caption[Example of the emulator modelling the Heaviside function with one data point close to the discontinuity, using a discontinous regressor function]
	{
		An Example of the emulator with a discontinuous prior. Note that the position of the discontinuity has to be known in order to define the regressor function. 
		
		If we were to know the location of the discontinuity, then everything changes; the current setup from Figures \ref{fig:emulatorexampleonhs} to \ref{fig:emulatorexampleonhs3} with the regressor and correlation function might have to change. To demonstrate this, let $\eta\left(x\right)=\left\{ \begin{array}{ccc}
		\frac{x}{2}-3 &  & x\leq0\\
		2x+5 &  & x>0
		\end{array}\right.$, and the regressor function to be $h\left(x\right)=\left\{ \begin{array}{ccc}
		0 &  & x<0\\
		1 &  & x\geq0
		\end{array}\right.$ (Note that at the discontinuity; $\eta$ jumps from $-3$ to $5$)
		
		As we can see from the graph, the domains outside of the data points of the discontinuity ($-3\leq x \leq 0$ and $1 \leq x \leq 3$) the emulator is a reasonable representation of $\eta$, however the emulator overshoots between $0$ and $1$.
	}
	\label{fig:emulatorexampleonhs5}
	\end{figure}
\end{landscape}

\newpage


\begin{landscape}
	\begin{figure}[ht]
	\centering
	\includegraphics[width=0.7\linewidth]{images/Emulator_example_on_HS_4}
	\caption[Example when using two data points on either side the discontinuity, using a discontinous regressor function]
	{
		Knowing that the discontinuity is at $0$ we obtain the above figure. Because the data points either side of the discontinuity are close together there is very little over/under shoot. This shows that, if the data points are "stitched" at the discontinuity; it could be suitable to use a regressor that contains the discontinuity at the stitched location. (as compared to Figure \ref{fig:emulatorexampleonhs3}). 
	}
	\label{fig:emulatorexampleonhs4}
	\end{figure}
\end{landscape}




\begin{landscape}
	\begin{figure}[ht]
		\centering
		\includegraphics[width=0.7\linewidth]{images/Emulator_example_on_HS_7}
		\caption[An example of using a discontinuous regressor to model the Heaviside function]
		{
			An example of using a discontinuous regressor to model the Heaviside function:
			
			It is observed that with this discontinuous regressor, the emulator is a better representation of the Heaviside function compared to the constant regressor. This is because information of the discontinuity has been provided to the emulator via the prior.
			
		}
		\label{fig:emulatorexampleonhs7}
	\end{figure}
\end{landscape}



\begin{landscape}
	\begin{figure}[ht]
		\centering
		\includegraphics[width=0.65\linewidth]{images/Emulator_example_on_HS_8}
		\caption[Example of using a discontinuous regressor to model the Heaviside function, assuming that the location of the discontinuity is unknown]
		{
			An example of using a discontinuous regressor to model the Heaviside function, assuming that the location of the discontinuity is unknown (legend is same as Figure \ref{fig:emulatorexampleonhs9}):
			
			If the location of the discontinuity is unknown, then the only information available is the data points. This figure demonstrates two extreme ends, assuming the discontinuity would be between the two data points in which "jumps" from $0$ to $1$. Comparing this figure  to Figure \ref{fig:emulatorexampleonhs9} it could be suggested that using a discontinuous regressor when the location of the discontinuity is unknown, does not significantly improve the emulator as modelling the Heaviside function.
			
			The relationship between Figures \ref{fig:emulatorexampleonhs8} and \ref{fig:emulatorexampleonhs8}b is more complicated than between Figures \ref{fig:emulatorexampleonhs} and \ref{fig:emulatorexampleonhs2}.
		}
		\label{fig:emulatorexampleonhs8}
	\end{figure}
\end{landscape}


\begin{landscape}
	\begin{figure}[ht]
		\centering
		\includegraphics[width=0.7\linewidth]{images/Emulator_example_on_HS_10}
		\caption[Example of using a discontinuous regressor that conflicts the data points to model the Heaviside function]
		{
			An example of using a discontinuous regressor that conflicts the data points to model the Heaviside function (legend is same as Figure \ref{fig:emulatorexampleonhs9}):
			
			When the data points are ignored to estimate where the discontinuity might be the emulator could result in something similar to the above figure, that shows that, any information conflicting with the prior will result in a high level of errors for the emulator causing over/under shooting.
			
			
		}
		\label{fig:emulatorexampleonhs10}
	\end{figure}
\end{landscape}


\newpage