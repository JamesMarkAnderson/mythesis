% !TeX spellcheck = <none>




\chapter{Research Method for Quantitative Analysis}\label{chap3}

\section{Introduction}

This chapter will discuss the approach to answering the thesis questions (Section \ref{sec:researchQuestions}) by introducing the assumptions made for the setup of the experiment and an outline of the experiments reported in Chapter \ref{chap4}. Throughout the experiments, goodness-of-fit techniques were used to measure how well the emulator handled simulators with step discontinuities. 

%\todo[inline]{have a paragraph about R in more detail here (remove footnote too)}
For the experiments R (A programming language commonly used in statistical computing) was used in conjunction with the BACCO package, which contains the Bayesian emulation formulas discussed in Section \ref{sec:BayesanEmulationAndNotations}.


\section{Research Approach}


	\subsection{Experiment Setup and Assumptions} \label{Experiment_Setup_Assumptions}
	
	The following sub-sections describe the components and assumptions required to set up the experiments for this thesis. 
	
	%\todo[inline]{RMD: Discuss about large datapoints in 1D, how A can be hard to inverse and in R: leading to use a numerically/approximation method, refer to Oakley2002 and they also discussed this problem}
	
%	NOTE TO SELF :see 3.2 and 3.3. Simulation design \cite{Oakley2002}
	
	 
	
	
	\subsubsection*{Regressor Function} \label{Experiment_Setup_Assumptions_regressor_Function}
	

	
	Figure \ref{fig:exampleofusinglinearprior} is an example of why a linear prior ($H\left[X\right]=\left[1,X\right]$) will not be used for the findings because beyond the data points, the prior has the main influence on the emulator due to the lack of information. Figure \ref{fig:exampleofusingconstantprior} is an example of the emulator with a constant as its prior. When there is a lack of information from the simulator's data points, the emulator's result will come from the prior. In this thesis a constant prior is used because of the lack of trend from the discontinuous functions.
	
	Because the step discontinuity functions have no trend, it could be suggested that for the Heaviside function, a regressor function  of $H\left[X\right]=\left[1\right]$ would be appropriate\footnote{The conclusion to use a no trend prior was made during the finding of optimising B in Appendix \ref{sec:OptimisingB}.}. The 1 is an arbitrary non-zero constant number as $\hat{\beta}$ (see equation \ref{eq:betaHat}) will be adjusted to best fit the data ($H\left[X\right]=\left[1,X\right]$ would also be equivalent to $H\left[X\right]=\left[100,2X\right]$).
	
	
	
	\begin{figure}[h]
		\centering
		\includegraphics[width=0.7\linewidth]{images/ExampleOfUsingLinearPrior}
		\caption{Example of the emulator using a linear prior}
		\label{fig:exampleofusinglinearprior}
	\end{figure}
	
	\begin{figure}[h]
		\centering
		\includegraphics[width=0.7\linewidth]{images/ExampleOfUsingConstantPrior}
		\caption{Example of the emulator using a constant prior}
		\label{fig:exampleofusingconstantprior}
	\end{figure}
	
	The emulator has no prior knowledge of the location of the discontinuity (see Section \ref{sssec:WhyNoDisRegessionFunction} on page \pageref{sssec:WhyNoDisRegessionFunction} for more details); this would mean that using a function with a discontinuity in the regressor would be unsuitable, because the prior ($\mathbf{h}\left(\mathbf{x}\right)^{T}\hat{\beta}$) is linear (see Section \ref{sssec:WhyNoDisRegessionFunction} for a detailed demonstration). The simulator itself is able to better approximate this discontinuity by running the simulation multiple times. However, this would take a considerable amount of time, whereas the emulator is quicker to run and should be able to give an estimation of the discontinuity's location.
	
	\subsubsection*{Sample Space and Distribution for the Data Points}
	
	During the experiments, data points  for the simulator were randomly drawn from a uniform distribution. This is because it is assumed that there is no (or very little) prior information on the location of the discontinuities, and therefore it is assumed the position of the discontinuities could be anywhere. However, it is assumed that there does exist some prior knowledge of the position of the discontinuities, this is to  minimise the number of runs of the emulators where the location of discontinuity does not exist, therefore a suitable range (typically between -3 and 3) is chosen where the probability of encountering the discontinuity is high.
	
	\subsubsection*{Sampling Data Points vs Sampling Discontinuity Positions}
	
	For this thesis, because the locations of the discontinuities are unknown, for the experiments the discontinuity was fixed; however, this information was not given to the emulator but was used in testing how well the emulator performed. If the data points were fixed and the position of the discontinuity were to change, there would be times when the emulator has repeating results. When there has been a change in the simulator but not in the data points output from the simulator, there would be no change in the emulator, as no new information has been given to it. Figure \ref{fig:exampleofwhydiscontinuityisntsampled1} demonstrates this, showing that there would be only a limit of six different emulators (or the number of data points plus one). Sampling data points provides more information about how well the emulator models discontinuities than sampling discontinuity positions.
	
	\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\linewidth]{images/ExampleOfWhyDiscontinuityIsntSampled_1}
	\caption{Examples of emulators if data points were fixed and Discontinuity Positions were sampled}
	\label{fig:exampleofwhydiscontinuityisntsampled1}
	\end{figure}
		
%	\begin{figure}
%	\centering
%	\includegraphics[width=0.7\linewidth]{images/ExampleOfWhyDiscontinuityIsntSampled_2}
%	\caption{Examples of emulators if data points were fixed and Discontinuity Positions were sampled}
%	\label{fig:exampleofwhydiscontinuityisntsampled2}
%	\end{figure}
	
	
	\subsubsection*{Correlation Function} \label{RMD:corrlationFuction}
	
	The correlation function used throughout the research is:
%	\todo[inline]{repeated}
	\begin{equation}
		c\left(x,x'\right)=e^{\left(x'-x\right)B\left(x'-x\right)}
	\end{equation}
	
	  This correlation function may be better suited for continuous functions and a more suitable correlation for discontinuity functions may be possible. However, due to the lack of research in modelling discontinuity functions with Bayesian emulation, this thesis will be focusing more upon the emulator and its well-researched methods, rather than adjusting the correlation function. This correlation function has been used in several research papers \cite{Caiado2015123,Chen2015291,Conti2009,Hankin2012,Kennedy2001,Montagna2013, Oakley2002,Zhang2015623} and will be used in this thesis to demonstrate the advantages and limitations of the Bayesian emulator.
	  
	   
	 
	%This correlation function is more suited for continuous functions and, because of this, it can be expected that it may reduce the accuracy of the emulator. Nevertheless, using this correlation function will demonstrate the advantages and limitations of the Bayesian emulator.
	
	$B$ from the correlation function is a diagonal matrix and describes how "rough" the function is. According to \citeA{Oakley1999} there is no analytical way of calculating $B$. 
	
	
	
	In this thesis, because the Heaviside has no length scale, $B$  will not be optimised, as the research focus is on modelling discontinuity functions with Bayesian emulation. Therefore $B$ in most cases will be equal to $1$ or an identity matrix in multiple dimensions. 
	
	A short experiment is reported in Appendix \ref{sec:OptimisingB} concluding that, to avoid an added complexity, for this research $B$ is best kept at $1$. If $B$ is too high, the correlation function will pull the emulator to the prior (similar to Figure \ref{fig:examplewhenbishigh}) and if $B$ is too low, the prior has little effect on the emulator, causing a high error for $\hat{\eta}$ outside the data point range and between the data points (as shown in Figure \ref{fig:examplewhenbishlow}).
				
	It should also be noted that the Heaviside function has no length scale, therefore having the data points scaled using the equation $X\sqrt{B}$ would provide equivalent emulators. For example, Figure \ref{fig:simpleexampleoftheemulatorconstainpriorbis1} is the output when the data points are $\left(-3,-2,0,2,3\right)$ and $B$ is 1, and Figure \ref{fig:simpleexampleoftheemulatorconstainpriorbis20scaleddatapoints} is the equivalent emulator, yet only the x-axis has been scaled, $B$ has been changed to 20 and the data points are scaled as $\left(\frac{-3}{\sqrt{20}},\frac{-2}{\sqrt{20}},0,\frac{2}{\sqrt{20}},\frac{3}{\sqrt{20}}\right)$. Because the data points for the experiments in this thesis can be scaled it was found it be best to let $B$ to be equal to 1.
	
	
	

	\begin{figure}
		\centering
		\includegraphics[width=0.7\linewidth]{images/SimpleExampleOfTheEmulatorConstainPriorBIs1}
		\caption{Example of emulator with contain prior and $B=1$ }
		\label{fig:simpleexampleoftheemulatorconstainpriorbis1}
	\end{figure}
	
	\begin{figure}
		\centering
		\includegraphics[width=0.7\linewidth]{images/simpleexampleoftheemulatorconstainpriorbis20ScaledDatapoints}
		\caption{Example of emulator with containt prior and $B=20$ and the data points scaled}
		\label{fig:simpleexampleoftheemulatorconstainpriorbis20scaleddatapoints}
	\end{figure}
	
	%\todo[inline]{Explain why $B$ is 1, Are we are assuming that B is known? Why can't we set B to be very low}
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{images/examplewhenbishlow}
	\caption[Example of when B from the correlation function is low]{Example of when B from the correlation function is low}
	\label{fig:examplewhenbishlow}
\end{figure}
	
	\begin{figure}
		\centering
		\includegraphics[width=0.7\linewidth]{images/ExampleWhenBIsHigh}
		\caption[Example of when B from the correlation function is high]{Example of when B from the correlation function is high}
		\label{fig:examplewhenbishigh}
	\end{figure}
	



%	\subsubsection*{\boldmath $m^{**}$ vs $\boldmath m^{*}$}
%%	\todo[inline]{I'm not really using $\sigma$ or $\hat{\sigma}$ so I should change my notation to $\hat{\eta}$ instead of $m^{*}$ or $m^{**}$}
%%	\todo[inline]{m star and m star star is to do with beta not $\sigma$}
%	From equation \ref{eq:Emulator-t} it is seen that the emulator contains a Student-t process: the number of degrees of freedom is unclear.  
%	Therefore $m^{*}$ will be used instead of $m^{**}$ and $\sigma$ will be incorporated in the covariance matrix.
%	For most of the research the variance was not taken to account, therefore the same results would be obtained regardless.
%	
	
	%\todo[inline]{Short discussion about $m^{**}$ and $m^{*}$ is yet to complete}
	%(Student's t: degree to freedom)...

	\subsection{Order of Experiment with the Emulator} \label{subsec:OrderOfExperiments}
	
	 
	This thesis will mainly be focusing using numerical experiments of the Bayesian emulator. The first part of the experiment will run the emulator modelling a simple discontinuity function, the Heaviside function, similar to Figure \ref{fig:simpleFunctionWithDiscontinuityheaviside}.
	
	The Heaviside function will be the first function which the emulator will model. Testing the emulator with the Heaviside function allows a basic understanding of the emulator's behaviour when modelling discontinuities and how the emulator is able to approximate where the discontinuity is. 
	If the emulator is unable to handle this simple function, then it can be expected that it will not be able to handle more complex functions with discontinuities. 
	
%	\begin{figure}
%		\centering
%		\includegraphics[width=0.7\linewidth]{images/simpleFunctionWithDiscontinuity.png}
%		\caption[Jump/step discontinuity function example]{Simple function with a jump discontinuity}
%		\label{fig:simpleFunctionWithDiscontinuity}
%	\end{figure}
	
	
	
	Next the dimensions of the function will be increased, containing a region where it is discontinuous. The findings from the one-dimensional case will be used to guide the research when applied to this simple two-dimensional case. A circle was chosen because it could be seen as a simple representation of the flow of a heavy gas at a fixed point in time. 
	
	
	Once an understanding of how the emulator deals with the two dimensional case the experiments will then extend this to three-dimensions. The third dimension will be time, using the same function as before, but having the region of the circle increase. It will be assumed that the gas is released from a source (e.g., a tank) continuously (not instantaneously), and because the tank's internal pressure will decrease as it releases the gas, the rate at which the gas escapes is proportional to the amount of gas remaining in the tank.  Using this assumption will create a simple function that could be represented as a heavy gas spreading outwards from a source.
	
	\newpage
	\subsection{Goodness of Fit} \label{subsec:GOF}
	
	
	%\footnote{For details of what $m^{*}\left(\mathbf{x}\right)$ and $\eta\left(0\right)$ please see section \ref{Literture_review_Bayesian_Emulation} and equations: \ref{eq:eta} \& \ref{eq:mstarstar}}
	
	
	
	This thesis has presented examples of goodness-of-fit techniques and will be used to measure the emulator's estimation to the discontinuous function. These results will be used to compare and evaluate how the emulator has been able to model the functions.

	Given $\mathbf{X}=\left\{ \mathbf{x}_{1},\mathbf{x}_{2},\ldots,\mathbf{x}_{n}\right\}$ is the set of training runs of the simulator containing $n$ observation points (or data points) from the simulator and $\textbf{x}_m$ is $m$ unique test points\footnote{These test points typically have not yet been observed by the simulator but will be used within the emulator.} that will be used to test the emulator and are ideally evenly spaced\footnote{If the test points are evenly spaced it might provide a more accurate result.}.	
	
	\[\textbf{x}=\left\{ x_{1},x_{2},\ldots,s_{m}\right\}\mbox{ where } x_{i+1} = x_{i} + \Delta, \Delta = \frac{B-A}{m}, x_{1}=A\mbox{ and } x_{m}=B\]
	
	
	Below are examples of goodness-of-fit techniques that will be used in this thesis. A probability density function (PDF) and a cumulative distribution function (CDF) will be produced for each technique. In Appendix \ref{Appendix:Eqations_and_proofs} it has been shown that an analytical solution would be difficult to obtain, and therefore these solutions were calculated numerically. 
	

	
	\begin{itemize}
		\item Mean Square Error (MSE): \\ 
		To find the MSE, the following formula is used:
		\begin{equation} \label{eq:MSE}
			\mbox{MSE}=\frac{1}{m}
			\stackrel[i=1]{m}{\sum}\left[\hat{\eta}\left(x_{i}\right)-\eta\left(x_{i}\right)\right]^{2}
		\end{equation}
		
		
		where $s_{i}$ is the $i$\textsuperscript{th} element of the test points that has not been observed by the simulator.
		
		As $m$ gets large the summation approaches into an integration:
		
		\begin{equation}
			\underset{m\rightarrow\infty}{\lim}\stackrel[i=1]{m}{\sum}\left[\hat{\eta}\left(x_{i}\right)-\eta\left(x_{i}\right)\right]^{2}=
			\int_{\ensuremath{\mathcal{D}}}\left[\hat{\eta}\left(x\right)-\eta\left(x\right)\right]^{2}dx
		\end{equation}
		
		Because the emulator: $\hat{\eta}$ will become more inaccurate outside the data point’s range. The MSE will only be measured between the most upper and lower boundary of the data points value. For example, if the data points are ${-5, 2.3, 0.1, 2.8, 7}$ then the MSE will be calculated between $-5$ and $7$, from the equation $\mathcal{D}$ is this range. By measuring the MSE along this region, the research will suggest how accurate the emulator is when modelling a discontinuous function. Ideally, a suitable accurate emulator will have a low error value. 
		
		\item Comparing $\hat{\eta}\left(d\right)$ to $\eta\left(d\right)$:\\
		For a Heaviside function; given the discontinuity is at $d$, it would be expected that  $\hat{\eta}\left(d\right)=\eta\left(d\right)=0.5$. This technique will also provide information of any under/overshooting.
		
		%Seeing where $m^{*}\left(\mathbf{x}\right)=0.5$ and compare $\mathbf{x}$ to the location of the discontinuity	
%		\todo[inline]{Work out a better title without using $\eta(d)$. As $\eta(d)$ is undefined/confusing/etc }
		\item Approximating and comparing the locations of the discontinuities from the emulator by solving $\hat{\eta}\left(x\right)=\eta(d)$ for $x$  where $d$ is the discontinuity position. \label{GOF:solvemxforx}\\
		
		
		
		It will be expected that when an accurate emulator will have $\hat{\eta}\left(d\right)=0.5$, for the Heaviside function (and the other functions that are to follow in this thesis). This is because, at the discontinuity, the result from true function may not exist (or at least an extreme difference on each side of the discontinuity) therefore it is assumed that the position of the discontinuity is at the midway point near the discontinuity such that: $\frac{\underset{x\rightarrow a^{+}}{\lim}\eta\left(x\right)+\underset{x\rightarrow a^{-}}{\lim}\eta\left(x\right)}{2}=\frac{1+0}{2}=0.5$. Therefore for this research, $\hat{\eta}\left(x\right)=0.5$ will be solved for $x$, and $x$ will be compared to the location of the discontinuity $d$.
		Ideally, a "good" emulator will have $x$ at the discontinuity or near the discontinuity with a low variance.
		
		\item Jaccard index\label{gof:BrayCurtis}
		
		To measure the accuracy of the emulator, equation \ref{Jaccard_PostProcess} will used to post-process the emulator. This will provide a percentage of overlapping from the simulator and the post-process emulator (equation \ref{{Jaccard_PostProcess}}). Region $D$ is not used in the equation, as by increasing the sample space the accuracy of the emulator can be easily influenced. \cite{real1996probabilistic}
		
		\begin{equation} \label{Jaccard_PostProcess}
			g(\mathbf{x})=\begin{array}{ccc}
			1 &  & \mbox{if }\hat{\eta}\left(\mathbf{x}\right)\geq0.5\\
			0 &  & \mbox{if }\hat{\eta}\left(\mathbf{x}\right)<0.5
			\end{array}
		\end{equation}
		
		
		This can also be represented as in table \ref{table:BrayCurtis}.
		
		\begin{table}[H]
			\centering
			\begin{tabular}{l|c|c|}
				\cline{2-3}
				& \multicolumn{1}{l|}{$\hat{\eta}\left(\mathbf{x}\right)\geq0.5$} & \multicolumn{1}{l|}{$\hat{\eta}\left(\mathbf{x}\right)<0.5$} \\ \hline
				\multicolumn{1}{|l|}{$\eta\left(\mathbf{x}\right)=1$} & B                      & A                      \\ \hline
				\multicolumn{1}{|l|}{$\eta\left(\mathbf{x}\right)=0$} & C                      & D                      \\ \hline
			\end{tabular}
			\caption{}
			\label{table:BrayCurtis}
		\end{table}
		
		$g(\mathbf{x})$ (from equation \ref{Jaccard_PostProcess}) will be compared to the simulator\footnote{Figures \ref{fig:1dim2disfunctionexample} (page \pageref{fig:1dim2disfunctionexample}) and \ref{fig:2D-Simple-Region} (page \pageref{fig:2D-Simple-Region}) are two examples used in this thesis}, and as a result it is assumed that the discontinuities exist the point $g(\mathbf{x})$ has a discontinuity (i.e. when $\hat{\eta}\left(\mathbf{x}\right)=0.5$). When solving $\mathbf{x}$ in $\hat{\eta}\left(\mathbf{x}\right)=\eta(\mathbf{d})$, a Jaccard index approach will also be used, by dividing the results from the emulator into four different regions\footnote{A visual representation of this can be found in Figures   \ref{fig:exampleOfRegionsBrayCurtis_RD} and \ref{fig:2DplotAprroximatingBoundardyExample_withLabels} (page \pageref{fig:exampleOfRegionsBrayCurtis_RD} and \pageref{fig:2DplotAprroximatingBoundardyExample_withLabels}).}: 
	
		\begin{itemize}
			\item Region \textbf{A}: where the simulator would output a $1$ and the emulator has \textbf{incorrectly} estimated it to be $0$
			\item Region \textbf{B}: where the simulator would output a $1$ and the emulator has \textbf{correctly} estimated it to be $1$
			\item Region \textbf{C}: where the simulator would output a $0$ and the emulator has \textbf{incorrectly} estimated it to be $1$
			\item Region \textbf{D}: where the simulator would output a $0$ and the emulator has \textbf{correctly} estimated it to be $0$
		\end{itemize}
		
		The equation $\frac{B}{A+B+C}$ will be used to measure how accurate the emulator.
		
		
		
		
		
		
%		\todo[inline]{Why is over/under shooting important? is it?}
		
		
		
%		\item Comparing $\frac{d\,m^{*}\left(0\right)}{d\mathbf{x}}$ to $\frac{d\,\eta\left(0\right)}{d\mathbf{x}}$\\
%		\todo[inline]{This is one GOF teq that I could remove. It hasn't given much benefit in the findings}
%		\todo[inline]{Make sure I rename the title and what not for this:}
		\item Investigating the steepness of $\hat{\eta}^{'}\left(0\right)$\\
		%Comparing $\left.\frac{d\hat{\eta}\mathbf{\left(x\right)}}{d\mathbf{x}}\right|_{\mathbf{x=0}}$ to $\left.\frac{d\eta\mathbf{\left(x\right)}}{d\mathbf{x}}\right|_{\mathbf{x=0}}$
		
		
		With the step-discontinuous functions, at the position of the discontinuity there is an extreme change in the function. Given the discontinuity is at $0$ it would be expected that steepness of $\hat{\eta}^{'}\left(0\right)$ (i.e. $\left.\frac{d\hat{\eta}\mathbf{\left(x\right)}}{d\mathbf{x}}\right|_{\mathbf{x=0}}
		$) are extreme numbers.
		
		
%	\todo[inline]{Missing some sort of conclusion}
%	\begin{itemize}
%		\item Research Questions
%		\item Research Apporch
%		\subitem Assumptions and setup
%		\subitem order of findings
%		\subitem GFMs
%	\end{itemize}
	
	In conclusion, this thesis will answer the above questions by using the developed goodness-of-fit methods on the described functions such as the Heaviside function, with some assumption which focuses on step-discontinuities.
		
%		\item Investigating $\stackrel[-\infty]{\infty}{\int}\left[m^{*}\left(\mathbf{x}\right)-\eta\left(\mathbf{x}\right)\right]^{2}d\mathbf{x}$ 
	\end{itemize}

\newpage
\begin{sidewaysfigure}[ht]
	\centering
	\includegraphics[width=0.7\linewidth]{images/exampleOfRegionsBrayCurtis}
	\caption[Example of Jaccard regions for one dimension with two discontinuities]{Example of Jaccard regions for one dimension with two discontinuities. note that the red line represents the approximation of where the discontinuous is based on the emulator (the black curved line) $\hat{\eta}=0.5$}
	\label{fig:exampleOfRegionsBrayCurtis_RD}
\end{sidewaysfigure}
\newpage
	
	
%	\todo[inline]{repeated text to fix}
%	\subsection{BACCO and R}
%	
%	The Bayesian emulator package for R, called BACCO, was designed by \citeA{Hankin2005} and was based on \citeauthor{Oakley1999}'s thesis \citeyear{Oakley1999}. For most of the experiments in this research, R will be used (using BACCO) to design and run the emulator. Examples of the code that generate the figures can be found in the appendix in Section \ref{RsampleCode}.
	
	
%	$m^{*}$ (see equation 3 \cite{Hankin2005}) was used rather than $m^{**}$ to avoid using the Student's t distribution, mainly because of simplicity and uncertainty in the research of the number of degrees of freedom for the Student's t distribution. 

\begin{comment}
\section{Research Data}



The emulator package for R called BACCO, was designed by \citeA{Hankin2005} and was based on the \citeauthor{Oakley1999}'s thesis \citeyear{Oakley1999}. For most of the experiments in the present research $m^{*}$ (see equation 3 \cite{Hankin2005}) was used rather than $m^{**}$ to avoid using the Student's t distribution, mainly because of simplicity and uncertainty in the research of the number of degrees of freedom for the Student's t distribution. Examples of the code that generate the figures can be found in the appendix in Section \ref{RsampleCode}



\todo[inline]{RMD: Check and delete bullet points}
\begin{itemize}
	\item How can the results be reproduced?
	
\end{itemize}

\begin{itemize}
	\item how the experiment was/will be done? (in order)
	\subitem Give an introduction about R and why R is used.
	\subitem Give an introduction about BACCO. What is it?
	\subsubitem Package 
	\subsubitem 	
	\subitem Other software or tools used and why?
	\subsubitem Matlab was used as a simple to sue 3D graphic. Information is imported to Matlab from R.
\end{itemize}

\begin{itemize}
	\item List thesis questions and aims
	\item how the experiment was done? (in order)
	\subitem why were these specific experimental procedures were chosen?
	\item What was done to answer the research question? Describe how it was done.
	\subitem justify the experimental design, explain how the results were analysed.
	
\end{itemize}


\end{comment}
